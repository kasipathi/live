#!/bin/bash

# migrate solo parameters to live
cp ~/turvo/ops/solo/stack/* ~/turvo/ops/live/us-west-2/development

# migrate common parameters to live
cp ~/turvo/ops/compute/parameters/common/* ~/turvo/ops/live/us-west-2/common
cp ~/turvo/ops/compute/components/discover/parameters/parameters.json ~/turvo/ops/live/us-west-2/common/common-logs.json
cp ~/turvo/ops/compute/components/metrics/parameters/* ~/turvo/ops/live/us-west-2/common

# migrate compute applications to live

environments=(
  common
  development
  production
  rehearsal
  stage
  union
  test
  learn
)

for environment in ${environments[@]} ; do

  # migrate parameters from compute applications
  cp ~/turvo/ops/compute/applications/activity/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/alerts/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/analytics/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/config/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/connect/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/discover/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/fusion/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/insights/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/launchpad/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/metrics/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/pay/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/performance/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/platform/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/rewards/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/scheduler/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/status/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null
  cp ~/turvo/ops/compute/applications/track/parameters/${environment}* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null

  # migrate mongo parameters from v1 folder
  cp ~/turvo/ops/compute/parameters/${environment}/${environment}.mongo.* ~/turvo/ops/live/us-west-2/${environment} 2>/dev/null

done
